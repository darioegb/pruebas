import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncrementadorComponent } from './incrementador.component';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { click } from './commons';
import { TitleCasePipe } from '@angular/common';

describe('IncrementadorComponent', () => {
  let component: IncrementadorComponent;
  let fixture: ComponentFixture<IncrementadorComponent>;
  let pipe = new TitleCasePipe(); 

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncrementadorComponent ],
      imports: [ FormsModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncrementadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Debe mostrar la leyenda en title case', () => {
    const leyenda = pipe.transform('progreso de carga');
    component.leyenda = leyenda;
    fixture.detectChanges();
    const elem: HTMLElement = fixture.debugElement.query(By.css('h3')).nativeElement;
    expect(elem.innerHTML).toContain(leyenda);
  });

  it('Debe mostrar  en input el valor del progreso', async(() => {
    component.cambiarValor(5);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const elem: HTMLInputElement = fixture.debugElement.query(By.css('input')).nativeElement;
      expect(elem.value).toBe('55');
    });
  }));

  it('Debe de in/decrementar en 5, con un click en el boton', () => {
    const botones = fixture.debugElement.queryAll(By.css('.btn-primary'));
    click(botones[0]);
    expect(component.progreso).toBe(45);
    click(botones[1]);
    expect(component.progreso).toBe(50);
  });

  it('En el titulo del componente, debe mostrar el progreso', () => {
    const botones = fixture.debugElement.queryAll(By.css('.btn-primary'));
    botones[0].triggerEventHandler('click', null);
    fixture.detectChanges();
    const elem: HTMLElement = fixture.debugElement.query(By.css('h3')).nativeElement;
    expect(elem.innerHTML).toContain('45');
  });
});
