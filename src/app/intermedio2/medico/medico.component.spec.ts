import { TestBed, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { MedicoComponent } from "./medico.component";
import { MedicoService } from './medico.service';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';


describe('Medico Component', () => {
    let componet: MedicoComponent;
    let service: MedicoService; 
    let fixture: ComponentFixture<MedicoComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ MedicoComponent ],
            providers: [ MedicoService ],
            imports: [ HttpClientModule ]
        });
        fixture = TestBed.createComponent(MedicoComponent);
        componet = fixture.componentInstance;
        service = TestBed.inject(MedicoService);
    })

    it('Debe crearse el componente', () => {
        expect(componet).toBeTruthy();
    });

    it('Debe retornar el nombre del medico', () => {
        const nombre = 'Juan';
        const res = componet.saludarMedico(nombre);
        expect(res).toContain(nombre);
    });

    it('Si no hay medicos debe traer un arreglo vacio', () => {
        spyOn(service, 'getMedicos').and.returnValue(of([]));
        componet.obtenerMedicos();
        expect(componet.medicos.length).not.toBeGreaterThan(0);
    });

    it('should get Date diff correctly in fakeAsync', fakeAsync(() => {
        const start = Date.now();
        tick(100);
        const end = Date.now();
        expect(end - start).toBe(100);
      }));
});