import { Routes } from '@angular/router';
import { HospitalComponent } from 'src/app/intermedio2/hospital/hospital.component';
import { MedicosComponent } from 'src/app/intermedio/espias/medicos.component';
import { IncrementadorComponent } from 'src/app/intermedio2/incrementador/incrementador.component';

export const RUTAS: Routes = [
    { path: 'hospital', component: HospitalComponent },
    { path: 'medico/:id', component: MedicosComponent },
    { path: '**', component: IncrementadorComponent }
];