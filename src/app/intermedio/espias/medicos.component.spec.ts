import { MedicosComponent } from './medicos.component';
import { MedicosService } from './medicos.service';
import { from, empty, throwError } from 'rxjs';


describe('MedicosComponent', () => {
    let component: MedicosComponent;
    const servicio = new MedicosService(null);

    beforeEach(() => {
        component = new MedicosComponent(servicio);
    });

    xit('Init: Debe de cargar los médicos', () => {
        const medicos = ['medico1', 'medico2', 'medico3'];

        spyOn(servicio, 'getMedicos').and.callFake(() => from([medicos]));
        component.ngOnInit();
        expect(component.medicos.length).toBeGreaterThan(0);
    });

    it('Debe de llamar al servidor para agregar un médico', () => {
        const espia = spyOn(servicio, 'agregarMedico').and.callFake(medico => empty());
        component.agregarMedico();
        expect(espia).toHaveBeenCalled();
    });

    it('Debe agregar un nuevo medico al arreglo de medicos', () => {
        const medico = { id: 1, nombre: 'Juan' };
        spyOn(servicio, 'agregarMedico').and.returnValue(from([medico]));
        component.agregarMedico();
        expect(component.medicos.indexOf(medico)).toBeGreaterThanOrEqual(0);
    });

    it('Si falla la adicion la propiedad mensajeError, debe ser igual al error del servicio', () => {
        const miError = 'No se pudo agregar el medico';
        spyOn(servicio, 'agregarMedico').and.returnValue(throwError(miError));
        component.agregarMedico();
        expect(component.mensajeError).toBe(miError);
    });

    it('Debe llamar al servidor para borrar a un medico', () => {
        spyOn(window, 'confirm').and.returnValue(true);
        const espia = spyOn(servicio, 'borrarMedico').and.returnValue(empty());
        component.borrarMedico('1');
        expect(espia).toHaveBeenCalledWith('1');
    });

    it('No Debe llamar al servidor para borrar a un medico', () => {
        spyOn(window, 'confirm').and.returnValue(false);
        const espia = spyOn(servicio, 'borrarMedico').and.returnValue(empty());
        component.borrarMedico('1');
        expect(espia).not.toHaveBeenCalledWith('1');
    });


});