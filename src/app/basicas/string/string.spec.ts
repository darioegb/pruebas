// describe('Pruebas de strings');
// it('Debe regresar un string');

import { message } from "./string";

describe('Pruebas de strings', () => {

    it('Debe de regresar un string', () => {
        const resp = message('Dario');
        expect(typeof resp).toBe('string');
    });

    it('Debe de retornar un saludo con el nombre enviado', () => {
        const nombre = 'Juan';
        const resp = message(nombre);
        expect(resp).toContain(nombre);
    });

});